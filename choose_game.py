from __future__ import print_function
import os
import random
import math
import sys

sys.path.insert(0, '.')
from constants import (OUR_GAME, ENTRIES, CUR_GAME, RATED, REPLAY, RATING,
                       BASE_RATING, DNW, DQ, CUR_GAME_PY3)


KEYS = ['fun', 'innovation', 'production']


def mean(seq):
    if seq:
        return sum(seq) / float(len(seq))
    return -1.0


def std(seq):
    if seq and len(seq) > 2:
        m = mean(seq)
        ss2 = [(x - m) * (x - m) for x in seq]
        return math.sqrt(sum(ss2) / float(len(seq) - 1))
    return -1.0


def print_stats(dStats):
    if 'info' in dStats:
        print(dStats['info'])
        return
    for key in KEYS:
        print('%s : %3d ' % (key, dStats[key]), end='')
    key = 'Ave'
    print('%s : %4.2f' % (key, dStats[key]))


def print_final_stats(dTotal):
    print("Total Statistics")
    for key in KEYS:
        if dTotal[key]:
            print('%-15s : %4.2f (std: %4.2f)   range: %d-%d' % (
                key, mean(dTotal[key]), std(dTotal[key]),
                min(dTotal[key]), max(dTotal[key])))
        else:
            print('No stats for %s' % key)
    print('')
    key = 'Ave'
    if dTotal[key]:
        print('%-15s : %4.2f (std: %4.2f)' % (
            key, mean(dTotal[key]), std(dTotal[key])))


def calc_exisiting_stats():

    print('Existing stats')
    dTotal = {'total': 0}
    dStats = {}
    for key in KEYS + ['Ave']:
        dTotal[key] = []
    played = os.listdir(RATED) + os.listdir(REPLAY)
    to_replay = os.listdir(REPLAY)
    for gamedir in played:
        if gamedir == OUR_GAME:
            continue
        filename = os.path.join(RATED, gamedir, RATING)
        if not os.path.exists(filename):
            filename = os.path.join(REPLAY, gamedir, RATING)
            if not os.path.exists(filename):
                print(gamedir, 'No stats')
                continue
        f = open(filename, 'rU')
        dStats[gamedir] = {}
        dTotal['total'] += 1
        fTot = 0
        for line in f:
            line = line.strip()
            if not line:
                continue
            if line.lower() == DNW.lower():
                dStats[gamedir]['info'] = DNW
                dTotal['total'] -= 1
                break
            elif line.lower() == DQ.lower():
                dStats[gamedir]['info'] = DQ
                dTotal['total'] -= 1
                break
            try:
                key, num = line.split(':')
                key = key.strip()
            except ValueError:
                key = line
            if key == 'info':
                break
            if key not in KEYS:
                raise RuntimeError('Unknown rating key "%s" in entry "%s"' %
                                   (key, gamedir))
            num = int(num.strip())
            dStats[gamedir][key] = num
            fTot += num
            dTotal[key].append(num)
        dStats[gamedir]['Ave'] = fTot / 3.0
        # Don't add unrated games to our total average
        if 'info' not in dStats[gamedir]:
            dTotal['Ave'].append(fTot / 3.0)
        f.close()
    pos = 1
    prev_ave = None
    for gamedir in sorted(dStats,
                          key=lambda x: dStats[x]['Ave'], reverse=True):
        if prev_ave:
            if prev_ave > dStats[gamedir]['Ave']:
                pos += 1
        prev_ave = dStats[gamedir]['Ave']
        print('%2d : %-15s' % (pos, gamedir), end='')
        print_stats(dStats[gamedir])
    if OUR_GAME not in played:
        print('%d games rated, %d games tried,'
              ' %d on the possible replay list' % (dTotal['total'],
                                                   len(played),
                                                   len(to_replay)))
        print('%s still listed as to be rated - please move it to Rated' % (
            OUR_GAME))
    else:
        print('%d games rated, %d games tried, %d on the possible replay'
              ' list, Our game (%s) ignored' % (
                  dTotal['total'], len(played) - 1, len(to_replay), OUR_GAME))
    fTot = 0
    print_final_stats(dTotal)


def fix_path(path, next_game, to_rate):
    """Update the path to point to the chosen game"""
    if os.path.lexists(path):
        if not os.path.exists(path):
            # Broken symlink, so replace
            os.unlink(path)
            full_path = os.path.join(ENTRIES, next_game)
            os.symlink(full_path, path)
        else:
            # Check if symlink is outdated
            real_game = os.path.realpath(path)
            unrated = [os.path.join(ENTRIES, x) for x in to_rate]
            if real_game in unrated:
                next_game = os.path.split(real_game)[1]
            else:
                print('cur_game symlink wrong, fixing')
                os.unlink(path)
                full_path = os.path.join(ENTRIES, next_game)
                os.symlink(full_path, path)
    else:
        # Create missing symlink
        full_path = os.path.join(ENTRIES, next_game)
        print(full_path, path)
        os.symlink(full_path, path)
    if not os.path.exists(os.path.join(path, RATING)):
        print('Adding base rating file')
        new_rating = open(os.path.join(path, RATING), 'w')
        base = open(BASE_RATING, 'r')
        new_rating.write(base.read())
        new_rating.close()
        base.close()
    return next_game


def find_next_game():
    try:
        to_rate = os.listdir(os.path.join(ENTRIES))
    except OSError:
        to_rate = []
    print('Still to rate: %d' % len(to_rate))
    if to_rate:
        next_game = random.choice(to_rate)
        next_game = fix_path(CUR_GAME, next_game, to_rate)
        next_game = fix_path(CUR_GAME_PY3, next_game, to_rate)
        print('Next game: %s' % next_game)
    else:
        print('All rated')


if __name__ == "__main__":
    calc_exisiting_stats()
    find_next_game()
