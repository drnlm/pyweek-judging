import os

# constants specific to the pyweek we're judging
PYWEEK = 'Pyweek_Date'
PYWEEK_NUM = -1
ENTRIES_DIR = 'pyweek-%d-all' % PYWEEK_NUM
OUR_GAME = "configure_this"

# Judging setup layout notes
# Directory stuff will live under
#BASEDIR = 'configure_this'
BASEDIR = '/home/neil/amusements/PyWeek'

# Directory for the juding VE
VE = 'VE'
# Python 3 VE
VE3 = 'VE3'
# ratting file name
RATING = 'rating'

# The judging layout
BASE = os.path.join(BASEDIR, PYWEEK)
ENTRIES = os.path.join(BASE, 'entries', ENTRIES_DIR)
RATED = os.path.join(BASE, 'entries', 'Rated')
REPLAY = os.path.join(BASE, 'entries', 'Replay')
BASE_RATING = os.path.join(BASE, 'entries', RATING)

CUR_GAME = os.path.join(BASE, VE, 'cur_game')
CUR_GAME_PY3 = os.path.join(BASE, VE3, 'cur_game')

# Constants used in the rating file
DNW = 'Did Not Work'
DQ = 'Disqualified'
