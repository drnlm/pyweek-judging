import sys
import os
import urllib2
from HTMLParser import HTMLParser

import optparse

sys.path.insert(0, '.')
from constants import ENTRIES_DIR

CHUNK = 50000


def parse_args(args):
    parser = optparse.OptionParser()
    parser.add_option('--debug', action="store_true", default=False,
                      dest="debug",
                      help="Debug mode - print urls, don't download anything")
    parser.add_option('--total', action="store_true", default=False,
                      dest="total",
                      help="Print total download sizes")
    parser.add_option('--force', action="store_true", default=False,
                      dest="force", help="Overwrite existing files")
    parser.add_option('--check-sizes', action="store_true", default=False,
                      dest="check",
                      help="Check downloaded size against the size content"
                      " length (no downloads are done)")

    return parser.parse_args(args)


class EntriesParser(HTMLParser, object):

    def __init__(self, opts):
        super(EntriesParser, self).__init__()
        self._urls = []
        self._parsing = False
        self._next_team = False
        self._in_team_name = False
        self._in_team = False
        self._team = None
        self._path = None
        self._debug = opts.debug

    def get_urls(self):
        return self._urls

    def handle_data(self, data):
        if self._in_team_name:
            if self._debug:
                print 'Making dir for team', self._team
            self._in_team_name = False
            self._in_team = True
            try:
                self._path = '%s/%s' % (ENTRIES_DIR, self._team)
                os.mkdir(self._path)
            except OSError:
                pass  # Ignore existing dir errors

    def handle_starttag(self, tag, attrs):
        if tag == 'div':
            if ('id', 'entries') in attrs:
                if self._debug:
                    print 'Found list start'
                self._parsing = True
        if tag == 'script' and self._parsing:
            if self._debug:
                print 'Reached list end'
            self._parsing = False
        if tag == 'div':
            if ('class', 'details') in attrs:
                self._next_team = True
                self._in_team_name = False
                self._in_team = False
            else:
                self._in_team = False
        if tag == 'a' and self._next_team:
            self._next_team = False
            self._in_team_name = True
            for tag, data in attrs:
                if tag == 'href':
                    self._team = data.replace('/e/', '').replace('/', '')
        elif tag == 'a' and self._in_team:
            for tag, data in attrs:
                if tag == 'href' and '/media/dl' in data:
                    url = data
                    filename = urllib2.unquote(url.split('/')[-1])
                    filename = '%s/%s' % (self._path, filename)
                    full_url = 'https://pyweek.org/%s' % url
                    full_url = self.unescape(full_url)
                    self._urls.append((full_url, filename))
                    if self._debug:
                        print "Added (%s - %s)" % (full_url, filename)


def downloader(remote, filename, length):
    with open(filename, 'wb') as outfile:
        total = 0
        last_per = 0
        if length:
            cont = True
            while cont:
                chunk = remote.read(CHUNK)
                total += CHUNK
                if chunk:
                    outfile.write(chunk)
                    per = int(float(total * 100) / length)
                    if per > last_per + 3:
                        print '.',
                        sys.stdout.flush()
                        last_per = per
                else:
                    cont = False
        else:
            print 'No content length'
            outfile.write(remote.read())
        if length:
            print ' Done (%d bytes)' % length
        else:
            print ' Done'


def process_urls(urls, opts):
    url_count = len(urls)
    total = 0
    for count, (url, filename) in enumerate(urls):
        print 'Downloading %s to %s (%d of %d)' % (url, filename,
                                                   count + 1, url_count)
        if opts.debug and not opts.total:
            continue
        if os.path.exists(filename) and not opts.force and not opts.check:
            print '%s exists, skipping' % filename
            continue
        remote = urllib2.urlopen(url)
        if hasattr(remote, 'info') and callable(remote.info):
            length = remote.info().getheader('Content-Length')
        else:
            length = None
        if length:
            length = int(length)
            total += length
        if opts.debug and opts.total:
            if length:
                print 'Download size: %s' % length
            else:
                print 'Content length unknown'
            continue
        if opts.check:
            if os.path.exists(filename):
                stat = os.stat(filename)
                if length:
                    print ('   file size : %d, content length: %d,'
                           ' difference: %d' % (stat.st_size, length,
                                                length - stat.st_size))
                else:
                    print 'file size: %d, content length unknown' % (
                        stat.st_size)
            else:
                print '    %s not downloaded yet' % filename
            continue
        # Actually download
        downloader(remote, filename, length)

    if opts.total:
        print 'Total download size: %s' % total


if __name__ == "__main__":
    opts, args = parse_args(sys.argv)

    if len(args) < 2:
        print 'Specify html file to extract entries from'
        sys.exit(1)

    with open(args[1]) as f:
        myParser = EntriesParser(opts)
        myParser.feed(f.read())

    urls = myParser.get_urls()

    process_urls(myParser.get_urls(), opts)
